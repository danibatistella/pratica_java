/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import static java.lang.Math.PI;

/**
 *
 * @author Danielli Batistella
 */
public class Circulo {
    double raio;  //variaveis
    double r;  
    double s;
        
    public Circulo(double praio,double pr,double ps) // construtor
    {
     raio = praio;
     r = pr;
     s = ps;
        }
    public double getArea() //metodo
    {
        double AC=PI*r*s;
        return AC;
    }
    
    public double getPerimetro()
    {
        double PC = 2*PI*raio;
        return PC;
    }
}

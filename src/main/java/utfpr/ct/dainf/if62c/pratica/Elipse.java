/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.awt.geom.Area;
import static java.lang.Math.PI;

/**
 *
 * @author Danielli Batistella
 */
public class Elipse {
    
    double r;  //variaveis
    double s;
    
    public Elipse(double pr, double ps) // construtor
    {
     r = pr;
     s = ps;
    }
    public double getArea() //metodo
    {
        double A=PI*r*s;
        return A;
    }
    
    public double getPerimetro()
    {
        double P = (PI*(3*(r + s))- (Math.sqrt((3*r+s)*(r+3*s))));
        return P;
    }
    
            
            
    
    
}

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Main {
    public static void main(String[] args) {
       Pratica41 p41 = new Pratica41();
       System.out.println(p41.area);
       System.out.println(p41.perimetro);
       System.out.println(p41.Carea);
       System.out.println(p41.Cperimetro);
            }
}
